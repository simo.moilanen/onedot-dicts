import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import { Provider } from 'react-redux'
import reducer from './reducers'
import { createStore } from 'redux'
import { saveState, loadState } from "./localStorage";
import throttle from 'lodash/throttle'
import './styles.css'


const persistedState = loadState();

const store = createStore(
  reducer,
  persistedState
);

store.subscribe(throttle(() => {
  saveState(store.getState())
}, 1000));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

