import { connect } from 'react-redux'
import DictionaryComponent from '../components/dictionary'
import { addRow, removeRow, updateRow} from "../actions/rows";
import { validate } from "../validate";


const mapStateToProps = state => {
  const dictionary = state.dictionaries.byId[state.dictionaries.activeId];
  return ({
    dictionary,
    rows: dictionary && validate(dictionary.rowIds.map(id => state.rows.byId[id]))
  })
};

const mapDispatchToProps = {
  addRow: addRow,
  removeRow: removeRow,
  updateRow: updateRow
};

export default connect(mapStateToProps, mapDispatchToProps)(DictionaryComponent)
