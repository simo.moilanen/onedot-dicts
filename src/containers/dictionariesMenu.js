import { connect } from 'react-redux'
import DictionariesMenuComponent from '../components/dictionariesMenu'
import { objectToArray } from "../helpers";
import { addDictionary, selectDictionary, removeDictionary } from "../actions/dictionaries";


const mapStateToProps = state => ({
  dictionaries: objectToArray(state.dictionaries.byId),
  activeId: state.dictionaries.activeId
});


const mapDispatchToProps = {
  addDictionary: addDictionary,
  selectDictionary: selectDictionary,
  removeDictionary: removeDictionary
};

export default connect(mapStateToProps, mapDispatchToProps)(DictionariesMenuComponent)
