import { rows } from "./rows";
import {addRow, removeRow, UPDATE_ROW, updateRow} from "../actions/rows";
import {removeDictionary} from "../actions/dictionaries";


describe('Rows reducer', () => {
  describe('ADD_ROW', () => {
    describe('when state is empty', () => {
      it('adds a new row', () => {
        const newState = rows(undefined, addRow({domain: 'Some key', range: 'Some value'}));
        const newId = Object.keys(newState.byId)[0];
        expect(newState).toEqual({
          byId: {
            [newId]: {id: newId, domain: 'Some key', range: 'Some value'}
          }
        })
      });
    });

    describe('when state is not empty', () => {
      let state = rows(undefined, addRow({domain: 'First', range: 'First value'}));
      state = rows(state, addRow({domain: 'Second', range: 'Second value'}));
      const [id1, id2] = Object.keys(state.byId);
      it('adds a new row after the first one', () => {
        expect(state).toEqual({
          byId: {
            [id1]: {id: id1, domain: 'First', range: 'First value'},
            [id2]: {id: id2, domain: 'Second', range: 'Second value'}
          }
        })
      });
    });
  });

  describe('REMOVE_ROW', () => {
    const state = rows({
      activeId: '',
      byId: {
        '1': {id: '1', domain: 'First', range: 'First value'},
        2: {id: 2, domain: 'Second', range: 'Second value'}
      }
    }, removeRow('1', 1));
    it('removes row defined by id', () => {
      expect(state).toEqual({
        activeId: '',
        byId: {
          2: {id: 2, domain: 'Second', range: 'Second value'}
        }
      })
    })
  });

  describe('UPDATE_ROW', () => {
    const state = rows({
      activeId: '',
      byId: {
        1: {id: 1, domain: 'First', range: 'First value'},
        2: {id: 2, domain: 'Second', range: 'Second value'}
      }
    }, updateRow({id: 2, domain: 'Second updated', range: 'Second value updated'}));
    it('updates row defined by id', () => {
      expect(state).toEqual({
        activeId: '',
        byId: {
          1: {id: 1, domain: 'First', range: 'First value'},
          2: {id: 2, domain: 'Second updated', range: 'Second value updated'}
        }
      })
    })
  });

  describe('REMOVE_DICTIONARY', () => {
    const state = rows({
      byId: {
        '1': {id: '1', domain: 'First', range: 'First value'},
        '2': {id: '2', domain: 'Second', range: 'Second value'}
      }
    }, removeDictionary({id: '1', title: 'First', rowIds: ['1']}));
    it('removes rows that belong to the removed dictionary', () => {
      expect(state).toEqual({
        byId: {
          '2': {id: '2', domain: 'Second', range: 'Second value'}
        }
      })
    })
  });

});
