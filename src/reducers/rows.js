import { ADD_ROW, REMOVE_ROW, UPDATE_ROW } from "../actions/rows";
import { REMOVE_DICTIONARY } from "../actions/dictionaries";


export function rows(state = {
  byId: {}
}, action) {
  switch (action.type) {
    case ADD_ROW:
      return {...state, byId: {...state.byId, [action.id]: {id: action.id, ...action.row}}};
    case REMOVE_ROW:
      const {[action.id]: deleted, ...newById} = state.byId;
      return {...state, byId: newById};
    case UPDATE_ROW:
      return {...state, byId: {...state.byId, [action.row.id]: action.row}};
    case REMOVE_DICTIONARY:
      let newRows = {...state.byId};
      action.dictionary.rowIds.forEach(d => delete newRows[d]);
      return {...state, byId: newRows};
    default:
      return state
  }
}