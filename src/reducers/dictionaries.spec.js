import { dictionaries } from "./dictionaries";
import { addDictionary, removeDictionary } from "../actions/dictionaries";
import { addRow, removeRow } from "../actions/rows";


describe('Dictionaries reducer', () => {
  describe('ADD_DICTIONARY', () => {
    describe('when state is empty', () => {
      it('adds a new dictionary and sets it active', () => {
        const newState = dictionaries(undefined, addDictionary('Some title'));
        const newId = Object.keys(newState.byId)[0];
        expect(newState).toEqual({
          activeId: newId,
          byId: {
            [newId]: {id: newId, title: 'Some title', rowIds: []}
          }
        })
      });
    });
    describe('when state is not empty', () => {
      let state = dictionaries(undefined, addDictionary('First'));
      state = dictionaries(state, addDictionary('Second'));
      const [id1, id2] = Object.keys(state.byId);
      it('adds a new dictionary after the first one', () => {
        expect(state).toEqual({
          activeId: id2,
          byId: {
            [id1]: {id: id1, title: 'First', rowIds: []},
            [id2]: {id: id2, title: 'Second', rowIds: []}
          }
        })
      });
    });
  });

  describe('REMOVE_DICTIONARY', () => {
    const state = dictionaries({
      activeId: 'some id',
      byId: {
        '1': {id: '1', title: 'First', rowIds: []},
        2: {id: 2, title: 'Second', rowIds: []}
      }
    }, removeDictionary({id: '1', title: 'First', rowIds: []}));
    it('removes dictionary defined by id', () => {
      expect(state).toEqual({
        activeId: 'some id',
        byId: {
          2: {id: 2, title: 'Second', rowIds: []}
        }
      })
    })
  });

  describe('ADD_ROW', () => {
    const state = dictionaries({
      activeId: '',
      byId: {
        1: {id: 1, title: 'First', rowIds: []},
        2: {id: 2, title: 'Second', rowIds: []}
      }
    }, addRow({}, 1));
    const newRowId = state.byId[1].rowIds[0]
    it('adds row id to correct dictionary rowIds field', () => {
      expect(state).toEqual({
        activeId: '',
        byId: {
          1: {id: 1, title: 'First', rowIds: [newRowId]},
          2: {id: 2, title: 'Second', rowIds: []}
        }
      })
    })
  });

  describe('REMOVE_ROW', () => {
    const state = dictionaries({
      activeId: '',
      byId: {
        1: {id: 1, title: 'First', rowIds: [1, 2, 3]},
        2: {id: 2, title: 'Second', rowIds: []}
      }
    }, removeRow(1, 1));
    it('removes row id from correct dictionary rowIds field', () => {
      expect(state).toEqual({
        activeId: '',
        byId: {
          1: {id: 1, title: 'First', rowIds: [2, 3]},
          2: {id: 2, title: 'Second', rowIds: []}
        }
      })
    })
  });
});
