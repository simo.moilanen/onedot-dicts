import {ADD_DICTIONARY, REMOVE_DICTIONARY, SELECT_DICTIONARY} from "../actions/dictionaries";
import { ADD_ROW, REMOVE_ROW } from "../actions/rows";


export function dictionaries(state = {
  activeId: '',
  byId: {}
}, action) {
  switch (action.type) {
    case ADD_DICTIONARY:
      return {activeId: action.id, byId: {...state.byId, [action.id]: {id: action.id, title: action.title, rowIds: []}}};
    case REMOVE_DICTIONARY:
      const {[action.dictionary.id]: deleted, ...newById} = state.byId;
      return {...state, byId: newById};
    case SELECT_DICTIONARY:
      return {...state, activeId: action.id};
    case ADD_ROW:
      const dictionary = state.byId[action.dictionaryId];
      return {...state, byId: {
          ...state.byId,
          [action.dictionaryId]: {...dictionary, rowIds: dictionary.rowIds.concat(action.id)}}
      };
    case REMOVE_ROW:
      const dict = state.byId[action.dictionaryId];
      return {...state, byId: {
          ...state.byId,
          [action.dictionaryId]: {...dict, rowIds: dict.rowIds.filter(rowId => rowId !== action.id)}}
      };
    default:
      return state
  }
}
