import { combineReducers } from 'redux'
import { dictionaries } from './dictionaries'
import { rows } from './rows'


export default combineReducers({
  dictionaries,
  rows
});
