

export const arrayToObjectById = arr => Object.assign({}, ...arr.map(item => ({[item.id]: item})));

export const objectToArray = obj => Object.keys(obj).map(key => obj[key]);
