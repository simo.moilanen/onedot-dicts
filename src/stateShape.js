// Initial planning of state shape

const stateShape = {
  dictionaries: {
    activeId: 1,
    byId: {
      1: {
        id: 1,
        title: "First dictionary",
        rowIds: [1]
      }
    }
  },
  rows: {
    byId: {
      1: {
        id: 1,
        domain: 'Some key',
        range: 'Some value'
      }
    }
  }
};
