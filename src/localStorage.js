

const stateName = 'onedot-dictionaries-state';

export const saveState = content => {
  try {
    localStorage.setItem(stateName, JSON.stringify(content))
  } catch (e) {
    // Ingore
  }
};

export const loadState = () => {
  try {
    const serializedItem = localStorage.getItem(stateName);
    if (serializedItem === null) {
      return undefined
    }
    return JSON.parse(serializedItem)
  } catch (e) {
    return undefined
  }
};