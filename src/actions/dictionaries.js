const uuidv1 = require('uuid/v1');


export const ADD_DICTIONARY = 'ADD_DICTIONARY';
export const REMOVE_DICTIONARY = 'REMOVE_DICTIONARY';
export const SELECT_DICTIONARY = 'SELECT_DICTIONARY';


export const addDictionary = title => ({
  type: ADD_DICTIONARY,
  id: uuidv1(),
  title
});

export const removeDictionary = dictionary => ({
  type: REMOVE_DICTIONARY,
  dictionary
});

export const selectDictionary = id => ({
  type: SELECT_DICTIONARY,
  id
});
