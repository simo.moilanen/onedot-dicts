const uuidv1 = require('uuid/v1');


export const ADD_ROW = 'ADD_ROW';
export const REMOVE_ROW = 'REMOVE_ROW';
export const UPDATE_ROW = 'SELECT_ROW';


export const addRow = (row, dictionaryId) => ({
  type: ADD_ROW,
  id: uuidv1(),
  row,
  dictionaryId
});

export const removeRow = (id, dictionaryId) => ({
  type: REMOVE_ROW,
  id,
  dictionaryId
});

export const updateRow = row => ({
  type: UPDATE_ROW,
  row
});
