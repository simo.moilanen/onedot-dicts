import { validate, FORK, DUPLICATE, CYCLE, CHAIN } from "./validate";


describe('Validate function', () => {
  const rows = [
    { domain: 'Dark grey', range: 'Dark'},
    { domain: 'Dark green', range: 'Green'}
  ];

  it('does not mutate the input parameter', () => {
    const returnValue = validate(rows);
    expect(returnValue).not.toEqual(rows);
    expect(rows[0]).not.toHaveProperty('errors');
  });

  describe('duplicate', () => {
    const rowsWithDuplicate = [
      { domain: 'Dark grey', range: 'Grey'},
      { domain: 'Dark grey', range: 'Grey'}
    ];
    it('adds error field to each conflicting row', () => {
      expect(validate(rowsWithDuplicate)).toEqual([
        { domain: 'Dark grey', range: 'Grey', errors: [DUPLICATE]},
        { domain: 'Dark grey', range: 'Grey', errors: [DUPLICATE]}
      ])
    });
  });

  describe('fork', () => {
    const rowsWithFork = [
      { domain: 'Dark grey', range: 'Dark'},
      { domain: 'Dark grey', range: 'Grey'}
    ];
    it('adds error field to each conflicting row', () => {
      expect(validate(rowsWithFork)).toEqual([
          { domain: 'Dark grey', range: 'Dark', errors: [FORK]},
          { domain: 'Dark grey', range: 'Grey', errors: [FORK]}
        ])
    });
  });

  describe('chain', () => {
    const rowsWithChain = [
      { domain: 'Dark grey', range: 'Grey'},
      { domain: 'Grey', range: 'Light Grey'}
    ];
    it('adds error field to each conflicting row', () => {
      expect(validate(rowsWithChain)).toEqual([
        { domain: 'Dark grey', range: 'Grey', errors: [CHAIN]},
        { domain: 'Grey', range: 'Light Grey', errors: [CHAIN]}
      ])
    });
  });

  describe('cycle', () => {
    const rowsWithCycle = [
      { domain: 'Dark Grey', range: 'Grey'},
      { domain: 'Grey', range: 'Dark Grey'}
    ];
    it('adds error field to each conflicting row', () => {
      expect(validate(rowsWithCycle)).toEqual([
        { domain: 'Dark Grey', range: 'Grey', errors: [CYCLE]},
        { domain: 'Grey', range: 'Dark Grey', errors: [CYCLE]}
      ])
    });
  });
});