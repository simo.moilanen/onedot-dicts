import React, { Component } from 'react';
import { Form, Button, Icon } from 'semantic-ui-react'


const initialState = {
  domain: '',
  range: ''
};

export default class NewRow extends Component {

  state = initialState;

  handleDomainInput = event => {
    this.setState({domain: event.target.value})
  };

  handleRangeInput = event => {
    this.setState({range: event.target.value})
  };

  handleSubmit = () => {
    const { addRow, dictionaryId } = this.props;
    const { domain, range } = this.state;
    addRow({domain, range}, dictionaryId);
    this.setState(initialState);
    document.getElementById("domain-input").focus();
  };

  render() {
    const {domain, range} = this.state;
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Group unstackable widths='2' >
          <Form.Input value={domain} onChange={this.handleDomainInput} id='domain-input' placeholder="New row..."/>
          <Form.Input value={range} onChange={this.handleRangeInput} action>
            <input/>
            <Button type='submit' color='green' icon>
              <Icon name='plus'/>
            </Button>
          </Form.Input>
        </Form.Group>
      </Form>
    )
  }
};
