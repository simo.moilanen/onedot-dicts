import React, { Component } from 'react'
import { Menu, Icon } from 'semantic-ui-react'
import NewDictionary from "./newDictionary"

export default class DictionariesMenu extends Component {

  handleRemoveButtonClick = (event, dictionary) => {
    const { removeDictionary } = this.props;
    if (window.confirm(`Are you sure you want to remove dictionary ${dictionary.title}`)) {
      removeDictionary(dictionary);
    }
    event.stopPropagation()
  };

  render() {
    const { dictionaries, activeId, selectDictionary, addDictionary } = this.props;
    return (
      <div className="dictionaries-menu">
        <h3 className="dictionaries-menu-heading">Dictionaries</h3>
        <Menu fluid vertical className="dictionaries-menu-menu">
          {dictionaries.map(dict => (
            <Menu.Item key={dict.id} name={dict.title} active={dict.id === activeId}
                       onClick={() => selectDictionary(dict.id)}>
              <Icon name='close' color='red' link onClick={e => this.handleRemoveButtonClick(e, dict)} />
              {dict.title}
            </Menu.Item>
          ))}
          <NewDictionary addDictionary={addDictionary} />
        </Menu>
      </div>
    )
  }
}
