import React from 'react'
import NewRow from './newRow'
import Row from "./row";
import { Grid } from 'semantic-ui-react'


const Dictionary = ({ dictionary, rows, addRow, removeRow, updateRow }) => {
  if (dictionary) {
    return (
      <div className="dictionary">
        <Grid className="dictionary-heading">
          <Grid.Column width={8}>
            <h3 className='dictionary-column-heading'>Domain</h3>
          </Grid.Column>
          <Grid.Column width={8}>
            <h3 className='dictionary-column-heading'>Range</h3>
          </Grid.Column>
        </Grid>
        {rows.map(row => (
          <Row key={row.id} row={row} updateRow={updateRow} removeRow={removeRow} dictionaryId={dictionary.id} />
        ))}
        <NewRow addRow={addRow} dictionaryId={dictionary.id} />
      </div>
    );
  } else return <div></div>
};

export default Dictionary;
