import React, { Component } from 'react';
import { Form, Icon, Label } from 'semantic-ui-react'
import { DUPLICATE, FORK, CHAIN, CYCLE } from "../validate";


const errorColors = {
  [DUPLICATE]:  'yellow',
  [FORK]:       'yellow',
  [CHAIN]:      'red'   ,
  [CYCLE]:      'red'
};

export default class Row extends Component {

  handleDomainInput = event => {
    const { updateRow, row } = this.props;
    const { errors: deleted, ...rowWithoutErrors } = row;
    updateRow({...rowWithoutErrors, domain: event.target.value})
  };

  handleRangeInput = event => {
    const { updateRow, row } = this.props;
    const { errors: deleted, ...rowWithoutErrors } = row;
    updateRow({...rowWithoutErrors, range: event.target.value})
  };

  handleRemove = () => {
    const { row, removeRow, dictionaryId } = this.props;
    if (window.confirm('Are you sure you want to remove this row?')) {
      removeRow(row.id, dictionaryId)
    }
  };

  render() {
    const { row } = this.props;
    return (
      <Form>
        {row.errors.length > 0 && row.errors.map((error, index) => (
          <Label key={index} pointing='below' color={errorColors[error]}>{error}</Label>
        ))}
        <Form.Group unstackable widths='2'>
          <Form.Input value={row.domain} onChange={this.handleDomainInput}/>
          <Form.Input value={row.range} onChange={this.handleRangeInput} icon={
            <Icon name='close' color='red' link onClick={this.handleRemove} />
          }/>
        </Form.Group>
      </Form>
  )
  }
};
