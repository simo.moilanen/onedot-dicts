import React from 'react';
import DictionariesMenu from '../containers/dictionariesMenu'
import { Container, Grid } from 'semantic-ui-react'
import Dictionary from "../containers/dictionary";


const App = () => (
  <div className="App">
    <div className="App-header"></div>
    <Container className="App-container">
      <Grid stackable>
        <Grid.Row>
          <Grid.Column width={4}>
            <DictionariesMenu/>
          </Grid.Column>
          <Grid.Column width={12}>
            <Dictionary/>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  </div>
);

export default App;
