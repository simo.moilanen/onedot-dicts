import React, { Component } from 'react';
import { Form, Button, Icon } from 'semantic-ui-react'


const initialState = {
  showInput: false,
  newTitle: ''
};

export default class NewDictionary extends Component {

  state = initialState;

  handleButtonClick = () => {
    this.setState({showInput: true})
  };

  handleInput = event => {
    this.setState({newTitle: event.target.value})
  };

  handleSubmit = () => {
    const { addDictionary } = this.props;
    const { newTitle } = this.state;
    addDictionary(newTitle);
    this.setState(initialState)
  };

  cancel = () => {this.setState(initialState)};

  render() {
    const { showInput, newTitle } = this.state;
    return (
      showInput ?
        <div>
          <Form onSubmit={this.handleSubmit}>
            <Form.Input placeholder="Title" value={newTitle} onChange={this.handleInput} icon={
              <Icon name='close' color='red' link onClick={this.cancel} />
            }/>
          </Form>
        </div>
        :
        <Button fluid color="green" onClick={this.handleButtonClick} >
          New dictionary
        </Button>
    )
  }
};
