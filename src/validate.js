export const DUPLICATE  = 'DUPLICATE';
export const FORK       = 'FORK';
export const CYCLE      = 'CYCLE';
export const CHAIN      = 'CHAIN';


export const validate = rows => {
  let ret = rows.map(r => ({...r, errors: []}));  // Copy the rows and add errors field
  let i, j;
  // Loop through each pair of rows:
  for (i = 0; i < ret.length; i++) {
    for (j = i + 1; j < ret.length; j++) {
      // Check if the pair has any of the errors:
      if (ret[i].domain === ret[j].domain) {
        if (ret[i].range === ret[j].range) {                                          // duplicate row
          ret[i].errors.push(DUPLICATE);
          ret[j].errors.push(DUPLICATE);
        } else {                                                                      // fork
          ret[i].errors.push(FORK);
          ret[j].errors.push(FORK);
        }
      }
      if (ret[i].domain === ret[j].range && ret[j].domain === ret[i].range) {         // cycle
        ret[i].errors.push(CYCLE);
        ret[j].errors.push(CYCLE);
      } else if (ret[i].domain === ret[j].range || ret[j].domain === ret[i].range) {  // chain
        ret[i].errors.push(CHAIN);
        ret[j].errors.push(CHAIN);
      }
    }
  }
  return ret;
};
