# Onedot dictionaries

Remote task when applying for a job at Onedot.

## Overview

This is a React app with Redux. The project was created with
[create-react-app](https://github.com/facebook/create-react-app), which
creates a React project with reasonable configuration.

In file `src/stateShape.js`, the Redux state shape is presented. The
state is saved to localStorage. Some components also have their own
internal state.

The dictionary is validated in the `Dictionary`-container. The
validation is simple, it checks every pair of rows in the dictionary and
marks the rows with errors id present. A cycle with three or more rows
is only identified as two chains. More advanced validation would include
for example identification of the other rows that causes the problem.

The reducers and validations were made with tests, which can be found in
*.spec.js files.

Semantic UI was used for styling.

## Installation

* Clone repository
* `npm install`

## Running tests

* `npm test`

## Development Server

* `npm start`

